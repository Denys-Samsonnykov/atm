<?php

require 'autoloader.php';

$loader = new Psr4AutoloaderClass();
$loader->addNamespace('App', __DIR__ . '/classes');
$loader->register();

$data = $_POST;

if (empty($data['balance']) || empty($data['realPIN']) || empty($data['withdraw']) || empty($data['verificationPIN'])) {
    echo 'Please enter all data';
}elseif(($data['balance'] < 0) || (!is_numeric($data['balance'])) ||
    ($data['withdraw'] < 0) || (!is_numeric($data['withdraw'])) ||
    ($data['realPIN']) < 1000 || ($data['realPIN']) > 9999 || !is_numeric($data['realPIN']) ||
    ($data['verificationPIN']) < 1000 || ($data['verificationPIN']) > 9999 || !is_numeric($data['verificationPIN'])) {
    echo 'Please enter correct data!';
    die();
}

if ($data['bank'] == 'Monobank') {
    $card = new \App\Banks\MonoBank($data['balance'], $data['realPIN']);
} elseif ($data['bank'] == 'Privatebank') {
    $card = new \App\Banks\PrivateBank($data['balance'], $data['realPIN']);
} else {
    $card = new \App\Banks\OtherBank($data['balance'], $data['realPIN']);
}

$handler = new \App\Handler($data['withdraw'], $data['verificationPIN']);

if(!$handler->checkoutPin($card)) {
    echo 'Invalid PIN';
}

if ($handler->getTotalWithdraw($card) > $card->getBalance()) {
    echo 'You don\'t have enough money';
    die();
}

$atm = new \App\ATMScript();

$cashes = $atm->getBills($handler->getWithdraw());

if(!empty($cashes)) {
    echo 'Please, take your money:'. '<br/><br/>';
    foreach ($cashes as $key => $cash) {
        if($cash > 1) {
            $bills = ' bills';
        }else{
            $bills = ' bill';
        }
        echo $key . ' => ' . $cash . $bills . '<br/>';
    }

    echo '<br/>' . 'Your current balance:  ' . $handler->getEndBalance($card) . '<br/>';
    echo '<br/>' . 'Transaction fee amounted to:  ' . $handler->getTransactionFee($card). '<br/>';
    echo '<br/>' . 'Please, do not forget your check and card! ';

}


