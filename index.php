<?php

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ATM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>
<body>
<h2 class="text-right">Our partners:</h2>
<p class="text-right">Monobank</p>
<p class="text-right">Private Bank</p>
<h1 class="text-center">Please, insert a card</h1>

<form action="result.php" method="post">
    <h2>Card imitation</h2>
    <div class="form-group col-md-4">
        <label>Enter your bank</label>
        <select class="form-control" name="bank">
            <option value="Monobank">Monobank</option>
            <option value="Privatebank">Private Bank</option>
            <option value="Otherbank">Other Bank</option>
        </select>
    </div>
    <div class="form-group col-md-4">
        <label>Enter your balance</label>
        <input type="text"  name="balance" class="form-control"  placeholder="9999">
    </div>
    <div class="form-group col-md-4">
        <label>Enter your PIN</label>
        <input type="text"  name="realPIN" class="form-control"  placeholder="****">
    </div>
    <br/>
    <h2>Verification data</h2>
    <div class="form-group col-md-4">
        <label>Enter the amount you want to withdraw</label>
        <input type="text" name="withdraw" class="form-control"  placeholder="8888">
    </div>
    <div class="form-group col-md-4">
        <label>Enter your PIN</label>
        <input type="text"  name="verificationPIN" class="form-control"  placeholder="****">
    </div>
    <button class="btn btn-primary">Submit</button>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>