<?php

namespace App;

class Card
{
    const BANK = 'OtherBank';

    private int $balance;
    private int $pin;

    public function __construct($balance, $pin)
    {
        $this->balance = $balance;
        $this->pin = $pin;
    }

    /**
     * @return mixed
     */
    public function getBalance() :int
    {
        return $this->balance;
    }

    /**
     * @return int
     */
    public function getPin(): int
    {
        return $this->pin;
    }
}