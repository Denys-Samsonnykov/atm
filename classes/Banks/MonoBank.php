<?php

namespace App\Banks;

use App\Card;

class MonoBank extends Card
{
    const BANK = 'MonoBank';
}