<?php

namespace App\Banks;

use App\Card;

class PrivateBank extends Card
{
    const BANK = 'PrivateBank';
}