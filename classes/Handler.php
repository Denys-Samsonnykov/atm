<?php

namespace App;

class Handler
{
    private int $withdraw;
    private int $pin;
    public  float $transactionFee = 0.07;

    public function __construct(int $withdraw, int $pin)
    {
        $this->pin = $pin;
        $this->withdraw = $withdraw;
    }

    /**
     * @return int
     */
    public function getWithdraw(): int
    {
        return $this->withdraw;
    }
    
    public function getTransactionFee(Card $card) : float
    {
        if($card::BANK == 'OtherBank') {
            $result = $this->transactionFee * $this->getWithdraw();
       }else{
            $result = 0;
        }
        return $result;
    }

    public function getTotalWithdraw(Card $card) :int
    {
        $total = $this->getWithdraw() + $this->getTransactionFee($card);

        return round($total);
    }

    public function checkoutPin(Card $card) : bool
    {
        if($card->getPin() == $this->pin) {
            $result =  true;
        }else{
            $result = false;
        }

        return $result;
    }

    public function getEndBalance(Card $card) : int
    {
        return $card->getBalance() - $this->getTotalWithdraw($card);
    }
}