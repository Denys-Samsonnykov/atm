<?php

namespace App;

class ATMScript
{
    public array $denominations = [5,10,20,50,100,200,500,1000];


    public function getBills($withdraw) : array
    {
        $billsAmount = array();
        if($withdraw % min($this->denominations) !== 0 ){
            echo 'This amount can`t be issued';
        }else {
            $denominations = array_reverse($this->denominations);
            foreach ($denominations as $denomination) {
                if ($withdraw >= $denomination) {
                    $count = floor($withdraw / $denomination);
                    $withdraw = $withdraw % $denomination;
                    $billsAmount[$denomination] = $count;
                }
            }
        }
        return $billsAmount;
    }


}